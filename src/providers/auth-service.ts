import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/pluck';
import {Observable} from 'rxjs/Observable';
import {Headers} from '@angular/http';
import {  Response } from '@angular/http';
import { Storage } from '@ionic/storage';


export class User {
  name: string;
  password: string;
  url: string;

  constructor(name: string, password: string, url: string) {
    this.name = name;
    this.password = password;
    this.url = url;
  }
}


/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthService {

  currentUser: User;
  data = '';

  constructor(public http: Http,private storage: Storage) {
    console.log('Hello AuthService Provider');
  }



  // Make a call to Get CSRF and check if we have access
  public getHTTP(credentials) {

        let ret = false;
        const headers: Headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(credentials.user + ':' + credentials.password));
        headers.append('Content-Type', 'application/json');
        console.log(headers);
        this.storage.set('Auth',btoa(credentials.user + ':' + credentials.password));
        this.storage.set('url', credentials.url);
        //return

        return (this.http.get('http://' + credentials.url +'/Windchill/servlet/rest/security/csrf', {
          headers: headers
        }).map((response: Response) =>  response.json())).
          map(x => {
            this.data = x.items[0].attributes.nonce;
            this.storage.set('CSRF', this.data);
            console.log('CSRF ->' + this.data);
            if(typeof this.data!='undefined' && this.data) {
              this.storage.set('CSRF', this.data);
              return ret = true;
            } }
          );
      }


  public login(credentials) {
       if (credentials.user === null || credentials.password === null || credentials.url === null ) {
         return Observable.throw("Please insert credentials ");
       }
        else
         {
             console.log('Here');
             let access = this.getHTTP(credentials);
             console.log('There');
                 this.storage.get('CSRF').then((val) => {
                  console.log('Your CSRF is', val);
                  });
                  this.storage.get('Auth').then((val) => {
                   console.log('Your Auth is', val);
                   });
                   this.storage.get('url').then((val) => {
                    console.log('Your url is', val);
                    });
             return Observable.create(observer => {
            observer.next(access);
            observer.complete();
         });
       }
  }

public getUserInfo() : User {
  return this.currentUser;
}

public logout() {
  return Observable.create(observer => {
    this.currentUser = null;
    observer.next(true);
    observer.complete();
  });
}

}
