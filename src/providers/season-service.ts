import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import { Storage } from '@ionic/storage';
import { OnInit} from '@angular/core';
import 'rxjs/add/observable/fromPromise';
import {Observable} from 'rxjs/Observable';


/*
  Generated class for the SeasonService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class SeasonService  implements OnInit {


  constructor(public http: Http, public storage: Storage) {
    console.log('Inside SeasonService constructor');
 }



  public getSeasonList(Auth:string, url:string) {


        const headers: Headers = new Headers();
        headers.append('Authorization', 'Basic ' + Auth);
        headers.append('Content-Type', 'application/json');
        return (this.http.get('http://' + url +'/Windchill/servlet/rest/rfa/instances?module=SEASON',
        {headers: headers}).
        map((response: Response) =>  response.json()));
      }


    ngOnInit() {

    }
}
