import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import { Storage } from '@ionic/storage';

/*
  Generated class for the ProductService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ProductService{

  constructor(public http: Http, private storage: Storage) {
    console.log('Hello ProductService Provider');
  }

  public getProductList(criteria: string, Auth:string, url: string) {
  const headers: Headers = new Headers();
  headers.append('Authorization', 'Basic ' + Auth);
  headers.append('Content-Type', 'application/json');
  return (this.http.get('http://' + url +'/Windchill/servlet/rest/rfa/instances?module=PRODUCT&criteria='+'*'+criteria+'*',
  {headers: headers}).
  map((response: Response) =>  response.json()));
}



}
