import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the Seasons page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-seasons',
  templateUrl: 'seasons.html',
})
export class Seasons {
  season: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {

    this.season = navParams.get('season');

    let url = storage.get('url');
    console.log(url);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Seasons');
  }

}
