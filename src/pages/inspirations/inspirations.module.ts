import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Inspirations } from './inspirations';

@NgModule({
  declarations: [
    Inspirations,
  ],
  imports: [
    IonicPageModule.forChild(Inspirations),
  ],
  exports: [
    Inspirations
  ]
})
export class InspirationsModule {}
